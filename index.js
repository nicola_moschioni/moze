const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
var bodyParser = require('body-parser');
const axios = require("axios");
const CarHelper = require("./helpers/CarHelper");
const Brand = require('./models/Brand');

require('./utils/db/MongooseConnection');

let app = express();


app.use(express.static(path.join(__dirname, 'public')))
    .use(bodyParser.urlencoded({
        extended: true
    }))
    .use(bodyParser.json())
    .set('views', path.join(__dirname, 'views'))
    .set('view engine', 'ejs')
    //.get('/', (req, res) => res.render('pages/index'))
    // All other GET requests not handled before will return our React app
    .listen(PORT, () => {
        console.log(`Listening on ${ PORT }`);
    });


app.get("/populateCars", async (req, res) => {

    const response = await axios({
        url: "https://run.mocky.io/v3/c8ee5d56-e003-4995-b138-a4eed9f60a6b",
        method: "get",
    });

    let results = [];


    for (const car of response.data.data) {
        await CarHelper.saveCar(car).then((res)=>{
            results.push({message: 'car added to db', car:car});
        }).catch((err)=>{
            results.push({ message: err });
        });
    }


    res.status(200).json(results);

});

app.get("/getCars", async (req, res) => {

    const brands = await Brand.find()
        .populate({path: 'cars'});
    res.status(200).json({cars: brands});

});

const root = require('path').join(__dirname, 'client', 'build')
app.use(express.static(root));
app.get("*", (req, res) => {
    res.sendFile('index.html', { root });
})