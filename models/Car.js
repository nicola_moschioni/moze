const mongoose = require("mongoose");
const {Schema} = require("mongoose");

const schema = new Schema({
    "model": String,
    "modelId": {
        type:String,
        required:true,
        unique: true
    },
    "modelDetailId":String,
    "modelDetail":String,
    "title":{
        type: String,
        required: true
    },
    "description": String,
    "availability": String,
    "promo":Boolean,
    "category": {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    "brand": {
        type: Schema.Types.ObjectId,
        ref: 'Brand',
        required: true
    }
})

module.exports = mongoose.model("Car", schema)
