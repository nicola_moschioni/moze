const mongoose = require("mongoose");
const {Schema} = require("mongoose");

const schema = new Schema({
    "brand": String,
    "brandId":{
        type:String,
        required:true,
        unique: true
    }
})


schema.virtual('categories', {
    ref: 'Category', //The Model to use
    localField: '_id', //Find in Model, where localField
    foreignField: 'brand', // is equal to foreignField
});

schema.virtual('cars', {
    ref: 'Car', //The Model to use
    localField: '_id', //Find in Model, where localField
    foreignField: 'brand', // is equal to foreignField
});

// Set Object and Json property to true. Default is set to false
schema.set('toObject', { virtuals: true });
schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model("Brand", schema)
