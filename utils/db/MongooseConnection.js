const mongoose = require('mongoose');
require('dotenv').config();

/**
 * Mongoose connection initializer
 */
class MongooseConnection
{

    constructor()
    {
        mongoose.connect(process.env.MONGO_DBURL,{
            useUnifiedTopology: true,
            useNewUrlParser: true,
            autoIndex: true
        }).then(()=>{
            console.info(
                `Successfully connected to ${process.env.MONGO_DBURL} MongoDB`,
            );
        }).catch(async (err)=>{
            if (err.message.code === 'ETIMEDOUT') {
                console.info('Attempting to re-establish database connection.');
                await mongoose.connect(process.env.MONGO_DBURL).catch((e)=>{
                    console.error('Error while attempting to connect to database:');
                    console.error(e);
                });
            } else {
                console.error('Error while attempting to connect to database:');
                console.error(err);
            }
        });
    }
}

module.exports = new MongooseConnection();