import React, { Component } from 'react';
import logo from '../logo_placeolder.png';
import carImg from '../car.png';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import {red} from "@mui/material/colors";

const carImgStyle = { width: "100%" };

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("/getCars")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.cars
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else if (items.length === 0){
            return <div>No data</div>;
        } else {
            return (
                <Box sx={{ flexGrow: 1 }}>
                    <Grid container>
                        {items.map(item => (
                            <Grid container>
                                <Grid item xs={10} marginBottom={2}>
                                    <Card sx={{ display: 'flex' }} elevation={0}>
                                        <CardMedia
                                            component="img"
                                            image={"/img/"+item.brand.toLowerCase()+".png"} onError={(e)=>{e.target.onerror = null; e.target.src=logo}}
                                            height="100"
                                            sx={{width:100}}
                                        />

                                        <CardContent sx={{ flex: '1 0 auto' }}>
                                            <h3>{item.brand}</h3>
                                        </CardContent>
                                    </Card>

                                    <Grid container spacing={2} marginTop={2} marginLeft={10}>
                                        {item.cars.map(car => (
                                            <Grid item xs={12} sm={6} md={2}>
                                                <img src={carImg} style={carImgStyle}/>
                                                <Paper elevation={0}>
                                                    {car.modelDetail}
                                                </Paper>
                                            </Grid>
                                        ))}
                                    </Grid>
                                </Grid>
                                <Grid item xs={2} textAlign={"center"} justifyContent={"center"}>
                                    <Card sx={{ display: 'flex' }} elevation={0}>
                                        <CardContent  sx={{ flex: '1 0 auto' }}>
                                            <h3>{"Vedi tutto >"}</h3>
                                        </CardContent>
                                    </Card>

                                </Grid>
                            </Grid>
                        ))}
                    </Grid>
                </Box>
            );
        }
    }
}

export default Content;