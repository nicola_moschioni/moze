#!/bin/sh
# wait-for-npm-install.sh

set -e

cmd="$@"

until [ -d "node_modules" ]; do
  >&2 echo "waiting for npm install"
  sleep 5
done

until [ ! -d "node_modules/.staging" ]; do
  >&2 echo "waiting for npm install"
  sleep 5
done

>&2 echo 'npm installed'

exec $cmd