require('dotenv').config();

const Car = require('../models/Car');
const Category = require('../models/Category');
const Brand = require('../models/Brand');

/**
 * Mongoose connection initializer
 */
class CarHelper
{
    static async saveCar(carData) {

        return new Promise(async (resolve, reject) => {


            let brand  = await Brand.findOne({brandId:carData.brandId}).exec();

            if(brand === null)
            {
                brand = new Brand({brandId:carData.brandId,brand: carData.brand});
                await brand.save().catch((error) => {
                    reject(error);
                });
            }

            let category = await Category.findOne({name:carData.category,brand:brand._id}).exec();

            if(category === null)
            {
                category = new Category({name: carData.category,brand: brand._id});
                await category.save().catch((error) => {
                    reject(error);
                });
            }

            let car = await Car.findOne({modelId:carData.modelId,brand:brand._id,category:category._id}).exec();

            if(car === null)
            {
                //validate data as required
                car = new Car({
                    model: carData.model,
                    modelId: carData.modelId,
                    modelDetailId: carData.modelDetailId,
                    modelDetail: carData.modelDetail,
                    title: carData.title,
                    description: carData.description,
                    availability: carData.availability,
                    promo: carData.promo,
                    category: category._id,
                    brand: brand.id
                });
                // book.publisher = publisher._id; <=== Assign user id from signed in publisher to publisher key
                await car.save().catch(async (err) => {
                    reject(err);
                });

            }




            resolve();
        });

    }

}

module.exports = CarHelper;